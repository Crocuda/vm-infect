# Infected configuration

This is a template configuration directory for machines created with
nixos-infect.

- First run nixos infect on a random freshly spawned linux machine, with or
  without network configuration detection depending on your host provider
- Then clone this repo and build configuration flake

## Usage

- Create root ssh credentials

```sh
ssh-keygen -t rsa -b 4096 -f ~/.ssh/id_rsa_remote
ssh-copy-id -i ~/.ssh/id_rsa_remote root@remote
```

- Infect the machine.

```sh
ssh root@remote -C \
"curl https://raw.githubusercontent.com/elitak/nixos-infect/master/nixos-infect | PROVIDER=hetznercloud NIX_CHANNEL=nixos-24.05 bash 2>&1 | tee /tmp/infect.log"
```

## Remote build (for small vm)

Clone this repo on your local machine.
Add nixos-infect generated files to the directory.

```sh
git clone https://gitea.com/Crocuda/vm-infect.git
scp root@remote:/etc/nixos/"*" ./vm-infect/
nixos-rebuild switch --target-host root@remote --flake path:.#vps
```

## Normal build

Add flake configuration

Then clone this repo to your config directory.

```sh
cd /etc/nixos/
nix-shell -p git
```

```sh
git init
git remote add origin https://gitea.com/Crocuda/vm-infect.git
git fetch
git pull
```

spawn a shell with needed packages and authorizations
then build the flake as a path to avoid process halting due to unstagged file.

```sh
nix-shell -p git --experimental-features 'nix-command flakes'
```

```sh
nixos-rebuild build --flake path:.#vps
```
