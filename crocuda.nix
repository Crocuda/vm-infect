{
  config,
  pkgs,
  inputs,
  ...
}: {
  crocuda = {
    users = ["anon"];

    keyboard.layout = "colemak-dh";

    terminal = {
      shell = {
        utils.enable = true;
        fish.enable = true;
      };
      file_manager.enable = true;
    };

    cicd.enable = true;

    # Servers
    # network.privacy.enable = true;

    servers = {
      ssh.enable = true;
      git = {
        # radicle.enable = true;
      };
      dns.enable = true;
      web = {
        jucenit.enable = false;
        sozu.enable = true;
      };
      mail.maddy = {
        enable = true;
        domains = [
          "crocuda.com"
          "areskul.com"
        ];
        accounts = [
          # "anon@areskul.com"
          "anon@crocuda.com"
          "areskul@areskul.com"
        ];
      };
    };

    virtualization = {
      docker.enable = true;
    };
  };
}
