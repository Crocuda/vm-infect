{
  config,
  pkgs,
  lib,
  inputs,
  ...
}: {
  # Enable Flakes
  nix.settings = {
    experimental-features = ["nix-command" "flakes"];
    auto-optimise-store = true;
    sandbox = "relaxed";
  };
  imports = [
    ./dns.nix
  ];

  environment.systemPackages = with pkgs; [
    curl
    git
    neovim
    fish

    # certs
    lego
    certbot
  ];

  environment.etc = {
    "sozu/config.toml".source = ./dotfiles/sozu/config.toml;
  };

  networking = {
    extraHosts = ''
      # prod
      # 127.0.0.1 crocuda.com
    '';
    firewall = {
      enable = true;
      # libvirt DHCP compatibility
      checkReversePath = "loose";
      allowedTCPPorts = [
        80
        443
        # smtp
        465
        587
        25
        # imap
        993
        143
      ];
      allowedUDPPorts = [53];
    };
  };
  users.users.anon.openssh.authorizedKeys.keys = config.users.users.root.openssh.authorizedKeys.keys;
}
