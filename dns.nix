{
  config,
  pkgs,
  lib,
  inputs,
  ...
}: {
  services = {
    unbound = {
      enable = false;
      enableRootTrustAnchor = true;
      resolveLocalQueries = false;
      checkconf = false;
      # package = pkgs.unbound;
      settings = {
        # easy start, stop and reload
        remote-control = {
          control-enable = true;
          control-interface = [
            "127.0.0.1"
            "::1"
          ];
        };
        # send minimal amount of information to upstream servers to enhance privacy
        server = {
          qname-minimisation = "yes";
          verbosity = 2;
          interface = ["0.0.0.0" "::0"];
          access-control = ["127.0.0.1" "::1"];
          do-ip4 = "yes";
          do-ip6 = "yes";
          do-udp = "yes";
          do-tcp = "yes";
          hide-identity = "yes";
        };

        # forward-zone = [
        #   {
        #     name = ".";
        #     forward-addr = "1.1.1.1@853#cloudflare-dns.com";
        #   }
        #   {
        #     name = "example.org.";
        #     forward-addr = [
        #       "1.1.1.1@853#cloudflare-dns.com"
        #       "1.0.0.1@853#cloudflare-dns.com"
        #     ];
        #   }
        # ];
      };
    };
    nsd = {
      hideVersion = true;
      enable = true;
      interfaces = ["0.0.0.0" "::0"];
      ipv4 = true;
      ipv6 = true;
      verbosity = 2;
      zones = {
        "crocuda.com." = let
          origin = "crocuda.com.";
          template =
            builtins.readFile
            dotfiles/nsd/template.zone;
        in {
          # dnssec = true;
          data =
            lib.strings.concatLines
            [
              ''
                $ORIGIN ${origin}
              ''
              template
            ];
        };
        "pipelight.dev." = let
          origin = "pipelight.dev.";
          template =
            builtins.readFile
            dotfiles/nsd/template.zone;
        in {
          # dnssec = true;
          data =
            lib.strings.concatLines
            [
              ''
                $ORIGIN ${origin}
              ''
              template
            ];
        };
        "areskul.com." = let
          origin = "areskul.com.";
          template =
            builtins.readFile
            dotfiles/nsd/template.zone;
        in {
          # dnssec = true;
          data =
            lib.strings.concatLines
            [
              ''
                $ORIGIN ${origin}
              ''
              template
            ];
        };
      };
    };
  };
}
